[![HitCount](http://hits.dwyl.io/Giftia/ChatDACS.svg)](http://hits.dwyl.io/Giftia/ChatDACS)
![Uptime Robot ratio (30 days)](https://img.shields.io/uptimerobot/ratio/m783632550-7da46d24226cb151b978c810?style=flat-square)
![Website](https://img.shields.io/website?label=demo&style=flat-square&up_message=online&url=http%3A%2F%2F39.108.239.49)
![GitHub release (latest by date)](https://img.shields.io/github/v/release/Giftia/ChatDACS?style=flat-square)
![GitHub](https://img.shields.io/github/license/Giftia/ChatDACS?style=flat-square)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/Giftia/ChatDACS?style=flat-square)
![GitHub stars](https://img.shields.io/github/stars/Giftia/ChatDACS?style=social)
![GitHub forks](https://img.shields.io/github/forks/Giftia/ChatDACS?style=social)
![GitHub followers](https://img.shields.io/github/followers/Giftia?style=social)
# 由于数据库格式改动，低版本升级至1.2.5及更新版本必须删除根目录下的db.db文件!

# ChatDACS
### 可聊天门禁系统(Chatable Door Access Control System)，一个简单的H5智能门禁系统，使用node.js编写。本项目为全端，与硬件端[ChaosNodeMCU](https://github.com/Giftia/ChaosNodeMCU/)联动为一个完整的门禁项目。远期计划完善为智能家居系统。低耦合，可脱离硬件端作为单独的聊天系统独立运行。
### [DEMO:http://39.108.239.49](http://39.108.239.49/)
![39.png](https://i.loli.net/2020/05/14/RgMvPI6NiowTrjC.png)
### 搭建教程与实装预览：[发表于知乎](https://zhuanlan.zhihu.com/p/67995935)
